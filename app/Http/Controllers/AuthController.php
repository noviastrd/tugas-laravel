<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
    	return view('register');
    }

    public function halo(Request $request){
    	return "Wellcome";
    }

    public function halo_post(Request $request){
    	$fnama = $request["fname"];
    	$lnama = $request["lname"];
       	return view('welcome' , compact('fnama','lnama'));
    }  
}
